<?php

/** 
 *
 * This module provides an option an entityreference term field to save the term lineage 
 *
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function entityreference_setlineage_ctools_plugin_directory($module, $plugin) {
  if ($module == 'entityreference' || $module == 'ctools') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_field_attach_form().
 *
 * Adds the regenerate lineage form fieldset.
 */
function entityreference_setlineage_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {

  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  $bundle = $wrapper->getBundle();
  $fields_info = field_info_instances($entity_type, $bundle);

  foreach ($fields_info as $field_name => $value) {
    $field_info = field_info_field($field_name);
    if ($field_info['type'] == 'entityreference' && $field_info['settings']['target_type'] == 'taxonomy_term') {
      if (isset($value['settings']['behaviors']['setlineage']) && $value['settings']['behaviors']['setlineage']['status'] && $value['settings']['behaviors']['setlineage']['lineage']) {
        $form[$field_name]['regen_lineage'] = array(
          '#type' => 'checkbox',
          '#title' => t('Regenerate term lineages'),
          '#default_value' => (isset($entity->{$field_name}['und'][0])) ? FALSE : TRUE,
          '#description' => t('Check this to generate the taxonomy term lineage.'),
          '#weight' => -1,
         );
      }
    }
  }
}

/**
 * 
 * Implements hook_field_attach_submit 
 *
 */
function entityreference_setlineage_field_attach_submit($entity_type, $entity, $form, &$form_state) {
  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  $bundle = $wrapper->getBundle();
  $fields_info = field_info_instances($entity_type, $bundle);
  
  foreach ($fields_info as $field_name => $value) {
    $field_info = field_info_field($field_name);
    if ($field_info['type'] == 'entityreference' && $field_info['settings']['target_type'] == 'taxonomy_term') {
      if (isset($value['settings']['behaviors']['setlineage']) && $value['settings']['behaviors']['setlineage']['status'] && $value['settings']['behaviors']['setlineage']['lineage']) {
        if ($form_state['input'][$field_name]['regen_lineage'] == 1) {
          $entity->{$field_name}['regen_lineage'] = TRUE;
        }
        else {
          $entity->{$field_name}['regen_lineage'] = FALSE;
        }
      }
    }
  }
}

/**
 * 
 * Implements hook_entity_presave 
 *
 */
function entityreference_setlineage_entity_presave($entity, $entity_type) {
  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  $bundle = $wrapper->getBundle();
  $fields_info = field_info_instances($entity_type, $bundle);

  foreach ($fields_info as $field_name => $value) {
    $field_info = field_info_field($field_name);
    if ($field_info['type'] == 'entityreference' && $field_info['settings']['target_type'] == 'taxonomy_term') {
      if (isset($value['settings']['behaviors']['setlineage']) && $value['settings']['behaviors']['setlineage']['status'] && $value['settings']['behaviors']['setlineage']['lineage'] && isset($entity->{$field_name}['regen_lineage']) && $entity->{$field_name}['regen_lineage']) {
        entityreference_setlineage_activate_parents($entity_type, $entity, $field_name);
      }
    }
  }
}

/**
 * 
 * Helper function to set all parent terms for all leaf terms that are set
 *
 */
function entityreference_setlineage_activate_parents($entity_type, $entity, $field_name) {
  $terms = field_get_items($entity_type, $entity, $field_name);
  if (empty($terms)) return;
  $tids = array();
  $parents = array();
  foreach($terms as $term) {
    $tids[] = $term['target_id'];
    $parent_objects = taxonomy_get_parents_all($term['target_id']);
    foreach ($parent_objects as $parent_term) {
      $parents[] = $parent_term->tid;
    }
  }
  $parents = array_unique($parents);
  $new_tids = array_diff($parents, $tids);
  foreach ($new_tids as $tid) {
    $entity->{$field_name}[LANGUAGE_NONE][] = array('target_id' => $tid);
  }
}
