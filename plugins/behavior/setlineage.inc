<?php

$plugin = array(
  'title' => t('Entity reference set lineage'),
  'description' => t('Add taxonomy term lineage for entity reference - applies only to taxonomy term reference.'),
  'class' => 'EntityReferenceSetLineageInstanceBehavior',
  'behavior type' => 'instance',
);
