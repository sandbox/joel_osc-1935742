<?php

class EntityReferenceSetLineageInstanceBehavior extends EntityReference_BehaviorHandler_Abstract {

  /**
   * Generate a settings form for this handler.
   */
  public function settingsForm($field, $instance) {
    $form['lineage'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add term lineage'),
    );
    return $form;
  }
}
